class Sorting
{
    public static void main (String[] args)
    {
        int a[] = {4, 9, 7, 5, 8, 9, 3};
        
        for(int j = 0; j<a.length; j++)
        {
            boolean swapped = false;
            int i = 0;
            while(i<a.length-1)
            {
                if (a[i] > a[i+1])
                {
                    System.out.print("[" + a[i+1] + "," + a[i] + "]" + "-> ");
                    int temp = a[i];
                    a[i] = a[i+1];
                    a[i+1] = temp;

                    for(int x = 0; x<a.length; x++)
                    {
                        System.out.print(a[x] + " ");
                    }
                    System.out.println("");

                    swapped = true;
                    i = 0;
                }
                i++;
            }
            
            if (!swapped)
                break;
        }
    }
}