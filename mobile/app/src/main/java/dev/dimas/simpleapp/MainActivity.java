package dev.dimas.simpleapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button btnReverse, btnUndoRedo;
    TextView tvOutput;
    EditText edtInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnReverse = findViewById(R.id.btnReverse);
        btnUndoRedo = findViewById(R.id.btnUndoRedo);
        tvOutput = findViewById(R.id.tvOutput);
        edtInput = findViewById(R.id.edtInput);
        TextUndoRedo helper = new TextUndoRedo(edtInput);

        btnUndoRedo.setOnClickListener(new DoubleClickListener() {
            @Override
            public void onDoubleClick() {
                helper.redo();
            }

            @Override
            public void onSingleClick() {
                helper.undo();
            }
        });

        btnReverse.setOnClickListener(v -> {
            tvOutput.setText("");
            String ag = edtInput.getText().toString().trim();
            String reverse = "";
            if(ag.length() != 0 ){
                char[] stringToCharArray = ag.toCharArray();
                for (int x = stringToCharArray.length - 1; x >= 0; x--){
                    reverse += stringToCharArray[x];
                }
                tvOutput.setText(reverse);
            }
        });

    }
}